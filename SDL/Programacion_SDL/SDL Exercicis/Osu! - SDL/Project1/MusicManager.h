#ifndef MUSICMANAGER_H
#define MUSICMANAGER_H
#include "Util.h"
#include "ResourceManager.h"
class MusicManager
{
public:
	
	~MusicManager();
	static MusicManager* getInstance(){
		if (mInstance == NULL)
		{
			mInstance = new MusicManager();
		}
		return mInstance;
	}
	
	

	void PauseMusic();
	void PlayMusic(int id, int loop);
	void ResumeMusic();
	void CloseMusic(); //Funcion para cerrar el programa
	void StopMusic(); //Parar totalmente los Sonidos
	ResourceManager* sResManager;
	int cancion; //Numero de la cancion de nuestro Reproductor
	void setCancion(int cancion){ this->cancion = cancion; } //Para asignar la canci�n
	int getCancion(){ return cancion; } //Recuperar el numero de la canci�n

private:
	MusicManager();
	static MusicManager* mInstance;
};
#endif

