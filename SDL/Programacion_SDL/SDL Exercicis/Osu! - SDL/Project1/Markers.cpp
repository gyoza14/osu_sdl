#include "Markers.h"


Markers::Markers()
{
}


Markers::~Markers()
{
}
void Markers::Init(int x, int y, int w, int h, int index, int aux, int duracion, int segundo, int color)
{
	this->RatioAcertado = 0;
	this->rect = { x, y, w, h };
	this->index = index;
	this->aux = aux;
	this->duracion = duracion;
	this->color = color;
	
}
void Markers::Update(){}
void Markers::Render(){}
HitMarker::HitMarker()
{
	
}
HitMarker::~HitMarker()
{

}
void HitMarker::Init(int x, int y, int w, int h, int index, int aux, int duracion, int segundo, int color)
{
	borde = ResourceManager::getInstance()->getGraphicID("Imagenes/circle.png");
	circuloR = ResourceManager::getInstance()->getGraphicID("Imagenes/circleR.png");
	circuloA = ResourceManager::getInstance()->getGraphicID("Imagenes/circleA.png");
	circuloL = ResourceManager::getInstance()->getGraphicID("Imagenes/circleL.png");
	circuloAm = ResourceManager::getInstance()->getGraphicID("Imagenes/circleAm.png");
	circuloV = ResourceManager::getInstance()->getGraphicID("Imagenes/circleV.png");
	this->alpha = 255;
	this->rect = { x, y, w, h };
	this->index = index;
	this->aux = aux;
	this->duracion = duracion;
	this->segundo = segundo;
	this->color = color;
	this->Aux_x = 6;
	this->Aux_y = 6;
	this->DestruirBoton = false;
	this->puntos = 0;
}

void HitMarker::Update()
{
	if (cManager->getClicked())
	{
		cManager->setClicked(false);

		if (gControler->x_cursor >= rect.x && gControler->x_cursor <= rect.x + rect.w)
		{
			if (gControler->y_cursor >= rect.y  && gControler->y_cursor <= rect.y + rect.h)
			{
				if (gControler->current_time >= segundo + duracion/2 )
				{
					if (Aux_x <= 1.5 && Aux_x >= 0.9)
					{
						cout << "300" << endl;
						DestruirBoton = true;
						RatioAcertado = 1;
						Destruido();
					}
					else if (Aux_x >= 1.5 && Aux_x <= 2)
					{
						cout << "100" << endl;
						DestruirBoton = true;
						RatioAcertado = 2;
						Destruido();
					}
					else if (Aux_x <= 3 && Aux_x > 2)
					{
						cout << "50" << endl;
						DestruirBoton = true;
						RatioAcertado = 3;
						Destruido();
					}
					else if (Aux_x < 0.9)
					{
						cout << "50" << endl;
						DestruirBoton = true;
						RatioAcertado = 3;
						Destruido();
					}
					
					//Si el aux es 1 == Perfect (1.5 - 1)
					//Si es mas peque� == te has passado
					//Si es mas grando == antes de tiempo	
				}
				else
				{
					cout << "Miss" << endl;
					DestruirBoton = true;
					RatioAcertado = 4;
					puntos = 0;
					Destruido();
				}
				
			}
		}
	}
}

bool Markers::Destruido()
{
	if (gControler->current_time >= segundo + duracion)
	{
		//cout << "DELETEAR un boton potente" << endl;
		RatioAcertado = 4;
		puntos = 0;
		return true;
	}
	else if (DestruirBoton)
	{
		if (RatioAcertado == 1)
		{
			puntos = 300;
		}
		else if (RatioAcertado == 2)
		{
			puntos = 100;
		}
		else if (RatioAcertado == 3)
		{
			puntos = 50;
		}
		else
		{
			puntos = 0;
		}
		return true;
		RatioAcertado = 0;
		DestruirBoton = false;
		
	}
	else
	{
		return false;
	}


	
}
void HitMarker::Render()
{
	switch (color)
	{
	case 1:
		vManager->renderTexture(circuloR,aux,0, rect.w, rect.h, rect.x, rect.y, 0, 0, 0);
		vManager->renderTextureAlpha(borde, aux, 0, rect.w * Aux_x, rect.h * Aux_y, rect.x +rect.h/2 - (rect.w/2)*Aux_x, rect.y +rect.w/2 - (rect.h/2)*Aux_y, 0, 0, 0, alpha);
		if (Aux_x >= 1)
		{
			Aux_x = 6 - ((gControler->current_time - segundo) / (duracion)) * 6;
			Aux_y = 6 - ((gControler->current_time - segundo) / (duracion)) * 6;
		}
		
		
		break;
	case 2:
		vManager->renderTexture(circuloV, aux,0,rect.w, rect.h, rect.x, rect.y,0, 0, 0);
		vManager->renderTextureAlpha(borde, aux, 0, rect.w * Aux_x, rect.h * Aux_y, rect.x + rect.h / 2 - (rect.w / 2)*Aux_x, rect.y + rect.w / 2 - (rect.h / 2)*Aux_y, 0, 0, 0, alpha);
		if (Aux_x >= 1)
		{
			Aux_x = 6 - ((gControler->current_time - segundo) / (duracion)) * 6;
			Aux_y = 6 - ((gControler->current_time - segundo) / (duracion)) * 6;
		}
		break;
	case 3:
		vManager->renderTexture(circuloA,aux,0, rect.w, rect.h, rect.x, rect.y, 0, 0, 0);
		vManager->renderTextureAlpha(borde, aux, 0, rect.w * Aux_x, rect.h * Aux_y, rect.x + rect.h / 2 - (rect.w / 2)*Aux_x, rect.y + rect.w / 2 - (rect.h / 2)*Aux_y, 0, 0, 0, alpha);
		if (Aux_x >= 1)
		{
			Aux_x = 6 - ((gControler->current_time - segundo) / (duracion)) * 6;
			Aux_y = 6 - ((gControler->current_time - segundo) / (duracion)) * 6;
		}
		
		break;
	case 4:
		vManager->renderTexture(circuloAm, aux,0,rect.w, rect.h, rect.x, rect.y, 0,0, 0);
		vManager->renderTextureAlpha(borde, aux, 0, rect.w * Aux_x, rect.h * Aux_y, rect.x + rect.h / 2 - (rect.w / 2)*Aux_x, rect.y + rect.w / 2 - (rect.h / 2)*Aux_y, 0, 0, 0, alpha);
		if (Aux_x >= 1)
		{
			Aux_x = 6 - ((gControler->current_time - segundo) / (duracion)) * 6;
			Aux_y = 6 - ((gControler->current_time - segundo) / (duracion)) * 6;
		}
		
		break;
	case 5: 
		vManager->renderTexture(circuloL, aux,0,rect.w, rect.h, rect.x, rect.y, 0, 0,0);
		vManager->renderTextureAlpha(borde, aux, 0, rect.w * Aux_x, rect.h * Aux_y, rect.x +rect.h/2 - (rect.w/2)*Aux_x, rect.y +rect.w/2 - (rect.h/2)*Aux_y, 0, 0, 0, alpha);
		if (Aux_x >= 1)
		{
			Aux_x = 6 - ((gControler->current_time - segundo) / (duracion)) * 6;
			Aux_y = 6 - ((gControler->current_time - segundo) / (duracion)) * 6;
		}
		
		break;
	default:
		break;
	}
	
	




}


