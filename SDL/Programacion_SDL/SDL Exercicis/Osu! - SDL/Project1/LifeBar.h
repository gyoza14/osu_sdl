#include "Util.h"
#include "Singletons.h"
class LifeBar
{
public:
	int barraVida;
	SDL_Rect rect;
	float vida;
	int aux;
	int Aux_x;
	void Init(int x, int y , int h, int w, int aux); 
	void Update();
	void Render();
	void setVida( float vida){ this->vida = vida; }
	float getVida(){ return vida; }

	LifeBar();
	~LifeBar();
};

