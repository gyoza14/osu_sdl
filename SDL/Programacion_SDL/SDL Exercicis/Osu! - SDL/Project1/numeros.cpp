#include "numeros.h"


void numeros::Init(int x, int y, int w, int h, int index, int aux, int duracion, int segundo)
{
	numero = ResourceManager::getInstance()->getGraphicID("Imagenes/numeros.png");

	this->rect = { x, y, w, h };
	this->index = index;
	this->aux = aux;
	this->duracion = duracion;
	this->segundo = segundo;

}

bool numeros::Destruido()
{
	if (gControler->current_time >= segundo + duracion)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void numeros::Update()
{
}

void numeros::Render()
{
	vManager->renderTexture(numero, aux,0,rect.w, rect.h, rect.x, rect.y-5,0, 0,0);
}

numeros::numeros()
{
}


numeros::~numeros()
{
}

int numeros::SetNumero(int numero)
{

	int number = 49 * numero;
	return number;

}
