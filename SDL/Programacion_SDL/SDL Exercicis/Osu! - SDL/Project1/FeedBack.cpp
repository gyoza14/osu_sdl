#include "FeedBack.h"

FeedBack::FeedBack()
{
}

void FeedBack::Init(int x, int y, int w, int h, int score, int segundo, int duracion)
{
	//const char * img = ("Imagenes/hit" + to_string(score) +".png").c_str();
	if (score == 0)
	{
		numero = ResourceManager::getInstance()->getGraphicID("Imagenes/hit0.png");
	}
	else if (score == 50)
	{
		numero = ResourceManager::getInstance()->getGraphicID("Imagenes/hit50.png");
	}
	else if (score == 100)
	{
		numero = ResourceManager::getInstance()->getGraphicID("Imagenes/hit100.png");
	}
	else if (score == 300)
	{
		numero = ResourceManager::getInstance()->getGraphicID("Imagenes/hit300.png");
	}

	this->rect = { x, y, w, h };
	this->puntuacion = score;
	this->duracion = duracion;
	this->segundo = segundo;
}


void FeedBack::Render()
{
	vManager->renderTexture(numero, 0, 0, rect.w, rect.h, rect.x, rect.y,0,0,0);
}


FeedBack::~FeedBack()
{
}

bool FeedBack::Destruido()
{
	
	if (gControler->current_time >= segundo + duracion)
	{
		return true;
	}
	else
	{
		return false;
	}
}

