#ifndef GC_H
#define GC_H
#include "Util.h"

class GameControler
{
public:
	static GameControler* getInstance(){
		if (gInstance == NULL)
		{
			gInstance = new GameControler();
		}
		return gInstance;
	}
	int array_cancion[99][215];
	int cancion;
	int dificultad;
	int lineas;
	float init_time;
	float current_time;
	int x_cursor;
	int y_cursor;
	void Mostrar_Cancion();
	~GameControler();

private:
	GameControler();
	static GameControler* gInstance;
};

#endif

