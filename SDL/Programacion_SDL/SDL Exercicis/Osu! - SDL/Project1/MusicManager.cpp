#include "MusicManager.h"
MusicManager* MusicManager::mInstance = NULL;

MusicManager::MusicManager()
{
	Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 1024);
	Mix_AllocateChannels(128);
	sResManager = ResourceManager::getInstance();
	cancion = 0;
}

void MusicManager::PauseMusic()
{
	
	Mix_Pause(-1);
}
void MusicManager::PlayMusic(int id, int loop)
{
	if (!Mix_Playing(-1)) //Si la canci�n esta en marcha no se ejecutar� de nuevo el PlayChannel
	{
		Mix_Chunk* music = sResManager->getAudioByID(id);
		Mix_PlayChannel(-1, music, loop);
	}
}
void MusicManager::ResumeMusic()
{
	
	Mix_Resume(-1);
}
void MusicManager::StopMusic()
{

	Mix_HaltChannel(-1); //Para el sonido de todos los Canales 
}
void MusicManager::CloseMusic()
{
	Mix_CloseAudio();
}

MusicManager::~MusicManager()
{
	
}
