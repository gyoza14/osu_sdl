#include "EndScreen.h"


void EndScreen::Init()
{
	
	GameOver = false;
	this->aux = 0;
	retry = ResourceManager::getInstance()->getGraphicID("Imagenes/retry.png");
	back = ResourceManager::getInstance()->getGraphicID("Imagenes/main_menu.png");
}

void EndScreen::Update()
{
	if (cManager->getClicked())
	{
		cManager->setClicked(false);
		if (cManager->Mouse_x >= 100 && cManager->Mouse_x <= 481) 
		{
			if (cManager->Mouse_y >= 600 && cManager->Mouse_y <= 690)
			{
				gControler->init_time = SDL_GetTicks() / 1000;
				sManager->Go_to(3);
				gControler->cancion = 1;
				tManager->Get_Song(gControler->cancion, gControler->dificultad);
			}
		}
		if (cManager->Mouse_x >= 600 && cManager->Mouse_x <= 981) 
		{
			if (cManager->Mouse_y >= 600 && cManager->Mouse_y <= 690)
			{
				sManager->Go_to(2);
			}
		}
	}
}

void EndScreen::Render()
{
	if (GameOver)
	{
		imgGO = ResourceManager::getInstance()->getGraphicID("Imagenes/GameOver.png");
		vManager->renderTexture(imgGO, 0, 0, 800, 450, (SCREEN_WIDTH/2)-400 , 30, 0, 0, 0);
	}
	else
	{
		imgGO = ResourceManager::getInstance()->getGraphicID("Imagenes/youWin.png");
		vManager->renderTexture(imgGO, 0, 0, 592, 200, (SCREEN_WIDTH / 2) - 296, 30, 0, 0, 0);
	}
	vManager->renderTexture(retry, 0, 0, 381, 90, 100, 600, 0, 0, 0);
	vManager->renderTexture(back, 0, 0, 381, 90,600 , 600, 0, 0, 0);
}

EndScreen::EndScreen()
{
}


EndScreen::~EndScreen()
{
}
