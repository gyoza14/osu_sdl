#include "VideoManager.h"
VideoManager* VideoManager::vInstance=NULL;

VideoManager::VideoManager(){
	gWindow = NULL;
	gScreenSurface = NULL;
	renderer = NULL;
	SDL_Init(SDL_INIT_EVERYTHING);
	gWindow = SDL_CreateWindow("Examen SDL", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	if (gWindow == NULL)
	{
		printf("Window could not be created! SDL Error: %s\n", SDL_GetError());

	}
	else
	{
		//Create renderer for window
		renderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED);
		if (renderer == NULL)
		{
			printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());

		}
		else
		{
			//Initialize renderer color
			SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);

			//Initialize PNG loading
			int imgFlags = IMG_INIT_PNG;
			if (!(IMG_Init(imgFlags) & imgFlags))
			{
				printf("SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError());
			}
		}
	}
	sResManager = ResourceManager::getInstance();

	

}
VideoManager::~VideoManager(){
	SDL_FreeSurface(gScreenSurface);
	SDL_DestroyWindow(gWindow);
	SDL_Quit();
	
}
void VideoManager::renderTexture(int img, int src_posX, int src_posY, int src_width, int src_height, int dst_posX, int dst_posY, double angle, int centerX, int centerY)
{
	SDL_Rect r, rectAux;
	rectAux.h = src_height;
	rectAux.w = src_width;
	rectAux.x = src_posX;
	rectAux.y = src_posY;
	r.x = dst_posX;
	r.y = dst_posY;
	r.w = src_width;
	r.h = src_height;
	
	SDL_Texture *origin = ResourceManager::getInstance()->getGraphicByID(img);

	SDL_Point center;

	center.x = centerX;
	center.y = centerY;
	
	SDL_RenderCopyEx(renderer, origin, &rectAux, &r, angle, &center, SDL_FLIP_NONE);

}

void VideoManager::renderTextureAlpha(int img, int src_posX, int src_posY, int src_width, int src_height, int dst_posX, int dst_posY, double angle, int centerX, int centerY,Uint8 alpha)
{
	SDL_Rect r, rectAux;
	rectAux.h = src_height;
	rectAux.w = src_width;
	rectAux.x = src_posX;
	rectAux.y = src_posY;
	r.x = dst_posX;
	r.y = dst_posY;
	r.w = src_width;
	r.h = src_height;

	SDL_Texture *origin = ResourceManager::getInstance()->getGraphicByID(img);

	SDL_Point center;

	center.x = centerX;
	center.y = centerY;
	SDL_SetTextureAlphaMod(origin, alpha);
	SDL_RenderCopyEx(renderer, origin, &rectAux, &r, angle, &center, SDL_FLIP_NONE);

}

void VideoManager::resizeTexture()
{


}


/*
void VideoManager::renderGraphicAlpha(int img, int posX, int posY, int width, int height, int rectAux_x, Uint8 alpha){
	SDL_Rect r, rectAux;
	r.x = posX;
	r.y = posY;
	rectAux.h = height;
	rectAux.w = width;
	rectAux.x = rectAux_x;
	rectAux.y = 0;
	
	SDL_Surface *origin = sResManager->getGraphicByID(img);
	sResManager->setAlphaGraphic(img, alpha);
	SDL_BlitSurface(origin, &rectAux, gScreenSurface, &r);
}*/
void VideoManager::clearScreen(unsigned int color_key){
	//Clear screen
	SDL_SetRenderDrawColor(renderer, color_key, color_key, color_key, color_key);
	SDL_RenderClear(renderer);
}
void VideoManager::updateScreen(){
	//Update screen
	SDL_RenderPresent(renderer);
}
void VideoManager::waitTime(int ms){
	SDL_Delay(ms);
}
void VideoManager::close(){
}