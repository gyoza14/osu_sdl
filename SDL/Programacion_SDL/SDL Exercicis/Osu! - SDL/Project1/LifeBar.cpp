#include "LifeBar.h"


void LifeBar::Init(int x, int y, int h, int w, int aux)
{
	this->rect = { x, y, w, h };
	this->aux = aux;
	barraVida = ResourceManager::getInstance()->getGraphicID("Imagenes/barraVida.png");
	vida = 700;

}

void LifeBar::Update()
{
	rect.w = vida - (SDL_GetTicks() - gControler->init_time) * 0.05;
	
}

void LifeBar::Render()
{
	vManager->renderTexture(barraVida, aux, 0, rect.w , rect.h, rect.x, rect.y, 0, 0, 0);
	

}

LifeBar::LifeBar()
{
}


LifeBar::~LifeBar()
{
}
