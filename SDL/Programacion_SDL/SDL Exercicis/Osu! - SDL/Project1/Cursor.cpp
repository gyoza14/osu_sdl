#include "Cursor.h"


void Cursor::Init(int x, int y, int w, int h, int aux)
{
	this->rect = {x , y, w, h };
	this->aux = aux;
	cursor = ResourceManager::getInstance()->getGraphicID("Imagenes/cursor.png");
}

void Cursor::Update()
{
	rect.x = cManager->Mouse_x;
	rect.y = cManager->Mouse_y;
}

void Cursor::Render()
{

	vManager->renderTexture(cursor,0,0, rect.w, rect.h, rect.x-25, rect.y-25, 0,rect.w/2,rect.h/2);
}

Cursor::Cursor()
{
}


Cursor::~Cursor()
{
}
