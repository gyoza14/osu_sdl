#ifndef VIDEOMANAGER_H
#define VIDEOMANAGER_H
#include "Util.h"
#include "ResourceManager.h"
class VideoManager
{
public:
	
	~VideoManager();
	static VideoManager* getInstance(){ 
		if (vInstance == NULL)
		{
			vInstance = new VideoManager();
		}
		return vInstance;
	}
	void renderTexture(int img, int src_posX, int src_posY, int src_width, int src_height, int dst_posX, int dst_posY, double angle, int centerX, int centerY);
	void renderTextureAlpha(int img, int src_posX, int src_posY, int src_width, int src_height, int dst_posX, int dst_posY, double angle, int centerX, int centerY,Uint8 alpha);
	void resizeTexture();
	void renderGraphicAlpha(int img, int posX, int posY, int width, int height, int rectAux_x, Uint8 alpha);
	void clearScreen(unsigned int color_key);
	void updateScreen();
	void waitTime(int ms);
	void close();
	SDL_Window* gWindow;
	SDL_Surface* gScreenSurface;
	SDL_Renderer* renderer;
	ResourceManager* sResManager;

private:
	VideoManager();
	static VideoManager* vInstance;
};
#endif