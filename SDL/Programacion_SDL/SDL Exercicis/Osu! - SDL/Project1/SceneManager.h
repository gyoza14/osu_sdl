#ifndef SCENEMANAGER_H
#define SCENEMANAGER_H
#include "Util.h"
class SceneManager
{
public:
	~SceneManager();
	static SceneManager* getInstance()
	{
		if (sInstance == NULL)
		{
			sInstance = new SceneManager;
		}
		return sInstance;
	
	}


	int scena;
	void Go_to(int scena);
	int current_Scene;


private:
	SceneManager();
	static SceneManager* sInstance;
};

#endif