#ifndef RESOURCEMANAGER_H
#define RESOURCEMANAGER_H
#include "Util.h"
class VideoManager;
//! ResourceManager class
/*!
Handles the load and management of the graphics in the game.
*/
class ResourceManager
{
public:

	//! Destructor.
	~ResourceManager();
	SDL_Surface* loadSurface(const char* path);
	//! Deletes a graphic from the ResourceManager map
	/*!
	\param file Filepath to the graphic
	*/
	void removeGraphic(const char* file);


	VideoManager* vResManager;
	//! Gets the graphic ID
	/*!
	\param file Filepath to the graphic
	\return ID of the graphic
	*/
	Sint32 getGraphicID(const char* file);


	//! Gets the graphic path given an ID graphic
	/*!
	\param ID of the graphic
	\return Filepath to the graphic
	*/
	std::string getGraphicPathByID(Sint32 ID);


	//! Returns width and Height of a Surface
	/*!
	*	\param img ID texture
	*	\param width Return variable for width value
	*	\param height Return variable for height value
	*/
	void getGraphicSize(Sint32 img, int &width, int &height);

	//! Returns width of a Surface
	/*!
	*	\param img ID texture
	* 	\return Width of Texture
	*/
	Uint16 getGraphicWidth(Sint32 img);

	//! Returns Height of a Texture
	/*!
	*	\param img ID texture
	*  \return Height of Texture
	*/
	Uint16 getGraphicHeight(Sint32 img);

	//! Returns the SDL_Surface of the graphic
	/*!
	\param ID ID of the graphic
	\return SDL_Surface
	*/
	SDL_Texture* getGraphicByID(Sint32 ID);


	Mix_Chunk* getAudioByID(Sint32 ID);
	Sint32 getAudioID(const char* file);
	Sint32 addMusic(const char* file);
	Mix_Chunk* loadMusic(const char* path);
	Uint32 updateFirstFreeSlotMusic();
	Mix_Chunk* getMusic(const char* file);
	//! Change general Alpha value to paint a concrete surface
	/*!
	\param ID ID of the graphic
	\param alpha_value From SDL_ALPHA_TRANSPARENT(0) to SDL_ALPHA_OPAQUE(255)
	*/
	void setAlphaGraphic(Sint32 ID, Uint8 alpha_value);

	//! Prints the path to loaded graphics
	void printLoadedGraphics();

	//! Create a new surface graphic to the ResourceManager
	/*!
	\param name for the graphic
	\param width Width for the graphic
	\param height Height for the graphic
	\return -1 if there's an error when loading
	*/
	Sint32 createGraphic(const char* file, Uint16 width, Uint16 height);

	//! Gets Singleton instance
	/*!
	\return Instance of ResourceManager (Singleton).
	*/
	static ResourceManager*  getInstance()
	{
		if (pInstance == NULL)
		{
			pInstance = new ResourceManager();
		}
		return pInstance;
	}

protected:
	//! Constructor of an empty ResourceManager.
	ResourceManager();

private:

	//! Adds a graphic to the ResourceManager 
	/*!
	\param file Filepath to the graphic
	\return -1 if there's an error when loading
	*/
	Sint32 addGraphic(const char* file);

	//! Searches in the ResourceManager and gets the graphic by its name. If it isn't there, loads it
	/*!
	\param file Filepath of the graphic
	\return SDL_Surface
	*/
	SDL_Surface* getGraphic(const char* file);

	//! Searches the Texture in the vector and returns its ID
	/*!
	\param img SDL_Surface of the graphic
	\return ID of the Texture
	*/
	Sint32 searchGraphic(SDL_Surface* img);

	//! Searches the first NULL in mGraphicsVector and updates mFirstFreeSlot to store its position
	/*!
	\return Index of the first NULL in mGraphicsVectorTexture
	*/
	Uint32 updateFirstFreeSlotGraphic();

	std::vector<SDL_Surface*>		mGraphicsVector;   /*!<  Vector that stores Surfaces. Useful for sequential access*/
	std::vector<Mix_Chunk*>		MusicVector;	
	vector<SDL_Texture*>		mTextureVector;	
	std::map<std::string, Sint32>		mIDMap;			/*!<  Map that stores ID. Links strings to ID, useful for sequential access*/
	std::map<std::string, Sint32>		MusicIDMap;
	Uint32					mFirstFreeSlot;		/*!<  First free slot in the mGraphicsVector*/
	Uint32					MusicFirstFreeSlot;
	static ResourceManager*			pInstance;		/*!<  Singleton instance*/
};

#endif
