#include "Util.h"
#include "Singletons.h"
class Score
{
public:
	float puntuacion;
	float aux_puntuacion;
	int numero;
	int index;
	int aux;
	SDL_Rect rect;
	void Init(int x, int y, int h, int w, int aux);
	void Update();
	void Render();
	void setScore(int _score);
	void incScore(int _score);
	int colocarNumero(int _score);
	Score();
	~Score();
};

