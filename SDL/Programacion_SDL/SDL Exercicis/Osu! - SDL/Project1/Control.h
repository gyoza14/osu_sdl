#ifndef CONTROLMANAGER_H
#define CONTROLMANAGER_H
#include "Util.h"


class Control
{
public:
	
	~Control();
	
	static Control* getInstance()
	{
		if (cInstance == NULL)
		{
			cInstance = new Control();
		}
		return cInstance;
	}


	int Mouse_x;
	int Mouse_y;
	void setExit(bool exit){ this->exit = exit;}
	bool getExit(){ return exit; }
	
	void setPause(bool pause){ this->pause = pause; }
	bool getPause(){ return pause; }
	void setClicked(bool click){ this->click = click; }
	bool getClicked(){ return click; }



	bool click;
	bool exit;
	bool pause;
	int ms;
	void Update();
private:


	Control();
	
	static Control* cInstance;
};
#endif

