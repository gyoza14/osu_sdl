#include "Dificultad.h"


void Dificultad::Init()
{
	Easy = ResourceManager::getInstance()->getGraphicID("Imagenes/Easy.png");
	Normal = ResourceManager::getInstance()->getGraphicID("Imagenes/Normal.png");
	Hard = ResourceManager::getInstance()->getGraphicID("Imagenes/Hard.png");
	Insane = ResourceManager::getInstance()->getGraphicID("Imagenes/Insane.png");

}


void Dificultad::Update()
{
	
	if (cManager->getClicked())
	{
		cManager->setClicked(false);

		if (cManager->Mouse_x >= 370 && cManager->Mouse_x <= 731) //Easy
		{
			if (cManager->Mouse_y >= 50 && cManager->Mouse_y <= 140)
			{
				gControler->dificultad = 1;
				sManager->Go_to(2);
				dificultad = 1;
				
				
			}
		}

		if (cManager->Mouse_x >= 370 && cManager->Mouse_x <= 731) //Normal
		{
			if (cManager->Mouse_y >= 250 && cManager->Mouse_y <= 340)
			{
				sManager->Go_to(2);

				
			}
		}


		if (cManager->Mouse_x >= 370 && cManager->Mouse_x <= 731) //Harderino
		{
			if (cManager->Mouse_y >= 450 && cManager->Mouse_y <= 540)
			{
				sManager->Go_to(2);

				
			}
		}

		if (cManager->Mouse_x >= 370 && cManager->Mouse_x <= 731) //Insaneino
		{
			if (cManager->Mouse_y >= 650 && cManager->Mouse_y <= 740)
			{
				sManager->Go_to(2);

				
			}
		}



	}


}


void Dificultad::Render()
{

	vManager->renderTexture(Easy, 0,0,381, 90, 370, 50, 0, 0, 0);
	vManager->renderTexture(Normal, 0,0,381, 90, 370, 250, 0, 0, 0);
	vManager->renderTexture(Hard, 0,0,381, 90, 370, 450, 0, 0, 0);
	vManager->renderTexture(Insane,0,0, 381, 90, 370, 650, 0, 0, 0);

}


Dificultad::Dificultad()
{
}


Dificultad::~Dificultad()
{
}
