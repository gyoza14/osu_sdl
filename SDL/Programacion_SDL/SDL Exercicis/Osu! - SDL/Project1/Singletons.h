#ifndef __Singletons__
#define __Singletons__

#include "VideoManager.h"
#include "ResourceManager.h"
#include "Control.h"
#include "MusicManager.h"
#include "SceneManager.h"
#include "TextManager.h"
#include "GameControler.h"

extern VideoManager *vManager;
extern ResourceManager *sResManager;
extern Control *cManager;
extern MusicManager *mManager;
extern SceneManager *sManager;
extern TextManager *tManager;
extern GameControler *gControler;


#endif