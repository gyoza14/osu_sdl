#pragma once
#include "Util.h"
#include "Singletons.h"
#include "Markers.h"
#include "numeros.h"
#include "LifeBar.h"
#include "FeedBack.h"

class Game
{
public:
	bool GameOver;
	void Init();
	void Update();
	void Render();
	Game();
	~Game();
	int dificultad;
	void setDificultad(int dificultad){ this->dificultad = dificultad; }
	int getDificultad(){ return dificultad; }
	void setGameOver(bool GameOver){ this->GameOver = GameOver; }
	bool getGameOver(){ return GameOver; }
	int linea = 0;
	void Instanciar_Boton(int x, int y, int tipo, int duracion);
	vector<numeros*>num;
	vector<numeros*>::iterator it2;
	vector<Markers*> marker;
	vector<Markers*>::iterator it;
	vector<FeedBack*>feedback;
	vector<FeedBack*>::iterator it3;
	LifeBar LB;
	int song1;

};

