#include "Util.h"
#include "Singletons.h"

class Markers
{
public:
	int x, y, h, w, duracion, index, aux, segundo,color;
	bool DestruirBoton;
	int RatioAcertado;
	SDL_Rect rect;
	Markers();
	~Markers();
	virtual void Init(int x, int y, int w, int h, int index, int aux, int duracion, int segundo, int color);
	virtual void Update();
	virtual void Render();
	Uint8 alpha;
	bool Destruido();
	float currentTime;
	int puntos;


	
};

class HitMarker : public Markers
{
public:
	HitMarker();
	~HitMarker();
	void Update();
	void Render();
	void Init(int x, int y, int w, int h, int index, int aux, int duracion, int segundo, int color);
	int circuloR, circuloA,circuloL,circuloV,circuloAm;
	float Aux_x, Aux_y;
	int borde;
	int numeros;
	int puntos;

};
class PhraseMarker : public Markers
{
public:
	PhraseMarker();
	~PhraseMarker();


};
class SpinMarker :public Markers
{
public:
	SpinMarker();
	~SpinMarker();


};

