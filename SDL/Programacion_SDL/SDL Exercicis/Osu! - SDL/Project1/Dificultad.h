#pragma once
#include "Util.h"
#include "Singletons.h"


class Dificultad
{
public:
	int Easy, Normal, Hard, Insane;
	int dificultad;
	void Init();
	void Update();
	void Render();
	Dificultad();
	~Dificultad();
};

