#include "Game.h"


Game::Game()
{
	dificultad = getDificultad();
}

void Game::Init()
{
	linea = 0;
	num.clear();
	marker.clear();
	feedback.clear();
	song1 = ResourceManager::getInstance()->getAudioID("Songs/hero.ogg");
	gControler->init_time = SDL_GetTicks();
	LB.Init(0, 0, 50, 700, 0);

}

void Game::Update()
{
	gControler->current_time = SDL_GetTicks() - gControler->init_time;
	switch (gControler->cancion)
	{
	case 1:
		mManager->PlayMusic(song1, -1);
		break;
	case 2:

		break;
	case 3:

		break;

	case 4:

		break;

	case 5:

		break;

	case 6:

		break;

	default:
		break;
	}

	if (linea < tManager->linea) 
	{
		if (gControler->current_time >= gControler->array_cancion[0][linea])
		{
			marker.push_back(new HitMarker());
			marker.back()->Init(gControler->array_cancion[1][linea] + 200, gControler->array_cancion[2][linea] + 200, 128, 128, gControler->array_cancion[3][linea], 0, gControler->array_cancion[4][linea], gControler->array_cancion[0][linea], gControler->array_cancion[6][linea]);
			num.push_back(new numeros());
			num.back()->Init(gControler->array_cancion[1][linea] + 243, gControler->array_cancion[2][linea] + 240, 49, 54, gControler->array_cancion[3][linea], num.back()->SetNumero(gControler->array_cancion[5][linea]), gControler->array_cancion[4][linea], gControler->array_cancion[0][linea]);
			linea++;
			if (gControler->current_time >= gControler->array_cancion[0][linea])
			{
				sManager->Go_to(4);
				mManager->StopMusic();
				setGameOver(false);
			}
		}

	}
	if (marker.size() > 0)
	{

		for (it = marker.begin(); it != marker.end(); it++)
		{
			(*it)->Update();
		}
	}

	if (num.size() > 0)
	{
		for (it2 = num.begin(); it2 != num.end(); it2++)
		{
			(*it2)->Update();
		}


	}

	LB.Update();

	if (LB.rect.w <= 0)
	{
		setGameOver(true);
		sManager->Go_to(4);
		mManager->StopMusic();
	}
}

void Game::Render()
{
	if (marker.size() > 0)
	{

		for (it = marker.begin(); it != marker.end(); it++)
		{
			(*it)->Render();

			if ((*it)->Destruido())
			{
				feedback.push_back(new FeedBack());
				feedback.back()->Init((*it)->rect.x - (*it)->rect.w / 2, (*it)->rect.y - (*it)->rect.h / 2, 256, 256, (*it)->puntos, gControler->current_time, 500);

				switch ((*it)->RatioAcertado)
				{
				case 1:
					if (LB.rect.w < 700)
						LB.vida += 40;
					break;
				case 2:
					if (LB.rect.w < 700)
						LB.vida += 10;
					break;
				case 3:
					if (LB.rect.w < 700)
						LB.vida += 5;
					break;
				case 4:
					LB.vida -= 30;
					break;
				default:
					break;
				}
				it = marker.erase(it);
				it = marker.begin();
				return;
			}


		}
		it = marker.begin();
	}


	if (num.size() > 0)
	{
		for (it2 = num.begin(); it2 != num.end(); it2++)
		{
			(*it2)->Render();

			if ((*it2)->Destruido())
			{

				it2 = num.erase(it2);
				it2 = num.begin();
				return;
			}

		}
		it2 = num.begin();
	}

	if (feedback.size() > 0)
	{
		for (it3 = feedback.begin(); it3 != feedback.end(); it3++)
		{
			(*it3)->Render();

			if ((*it3)->Destruido())
			{

				it3 = feedback.erase(it3);
				it3 = feedback.begin();
				return;
			}

		}
		it3 = feedback.begin();
	}



	if (num.size() > marker.size())
	{
		for (it2 = num.begin(); it2 != num.end(); it2++)
		{
			it2 = num.erase(it2);
			it2 = num.begin();
			return;
		}
		it2 = num.begin();
	}
	LB.Render();

}
Game::~Game()
{

}


