#include "Util.h"
#include "Menu.h"
#include "Dificultad.h"
#include "MapaMundi.h"
#include "Game.h"
#include "Pause.h"
#include "Cursor.h"
#include "Singletons.h"
#include "EndScreen.h"


int main(int argc, char* args[])
{	
	bool fin = false;
	Menu menu;
	Dificultad dificultad;
	MapaMundi mapa;
	Game game;
	Pause pause;
	EndScreen end;
	cManager = Control::getInstance();
	vManager = VideoManager::getInstance();    
	sManager = SceneManager::getInstance();
	gControler = GameControler::getInstance();
	tManager = TextManager::getInstance();
	mManager = MusicManager::getInstance();
	menu.Init();
	dificultad.Init();
	mapa.Init();
	end.Init();

	Cursor cur;
	cur.Init(0,0,50,50,0);

	while (!fin)
	{
		vManager->clearScreen(0x0000);
		cManager->Update();
		cur.Update();
		gControler->x_cursor = cur.rect.x;
		gControler->y_cursor = cur.rect.y;
		
		if (cManager->getExit())
		{
			if (sManager->scena == 0)
			{
				fin = true;
			}
			else
			{
				if (sManager->scena == 3)
				{
					mManager->StopMusic();
				}
				sManager->scena = sManager->scena - 1;
				cManager->setExit(false);

			}
		}
		
		
		switch (sManager->scena)
		{
		case -1:
			fin = true;
			break;

		case 0:
			sManager->current_Scene = 0;
			menu.Update();
			menu.Render();			
			break;

		case 1:
			sManager->current_Scene = 1;
			dificultad.Update();
			dificultad.Render();		
			break;

		case 2:
			sManager->current_Scene = 2;
			mapa.Update();
			mapa.Render();
			break;

		case 3:
			if (sManager->current_Scene != 3)
			{
				game.Init();
				sManager->current_Scene = 3;
			}
			
			game.Update();
			game.Render();
			game.setDificultad(dificultad.dificultad);

		
			break;
		case 4:
			sManager->current_Scene = 4;
			end.GameOver = game.getGameOver();
			end.Update();
			end.Render();
		default:
			break;
		}
		cur.Render();
		vManager->updateScreen();
	}
	
	return 0;


}
