#ifndef TM_H
#define TM_H
#include "Util.h"
#include "GameControler.h"
//#include "Singletons.h"

class TextManager
{
public:
	~TextManager();
	static TextManager* getInstance(){
		if (tInstance == NULL)
		{
			tInstance = new TextManager();
		}
		return tInstance;
	}
	int linea;
	int columna;
	GameControler* gGManager;
	
	void Get_Song(int song, int dificultad);
	
private:
	TextManager();
	static TextManager* tInstance;
};

#endif