#include "ResourceManager.h"
#include "VideoManager.h"
ResourceManager* ResourceManager::pInstance=NULL;



ResourceManager::ResourceManager()
{
	
	
	mFirstFreeSlot = 0;
	MusicFirstFreeSlot = 0;

	

}

SDL_Surface* ResourceManager::loadSurface(const char* path)
{
	SDL_Surface* surface = IMG_Load(path);
	if (surface == NULL)
	{
		cout << "Error LoadSurface" << endl;
	}
	return surface;

}


Sint32 ResourceManager::getGraphicID(const char* file)
{
	Sint32 id = -1;
	std::map<std::string, Sint32>::iterator it = mIDMap.find(file);
	if (it != mIDMap.end())
	{
		id = it->second;
	}
	else
	{
		
		id = addGraphic(file);
		mIDMap.insert(std::pair<std::string, Sint32>(file, id));
	}
	return id;
	
}
void ResourceManager::getGraphicSize(Sint32 img, int &width, int &height)
{
	width = getGraphicWidth(img);
	height = getGraphicHeight(img);
}
Uint16 ResourceManager::getGraphicWidth(Sint32 imgId)
{
	return mGraphicsVector[imgId]->w;

}
SDL_Surface* ResourceManager::getGraphic(const char* file)
{
	map<string, Sint32>::iterator it = mIDMap.find(file);

	if (it != mIDMap.end()){
		Sint32 pos = it->second;
		return mGraphicsVector[pos];
	}
	return mGraphicsVector[addGraphic(file)];
}
Sint32 ResourceManager::searchGraphic(SDL_Surface* img)
{
	Sint32 id=-1;
	return id;
}
Uint16 ResourceManager::getGraphicHeight(Sint32 imgId)
{
	return mGraphicsVector[imgId]->h;
}
SDL_Texture* ResourceManager::getGraphicByID(Sint32 ID)
{
	return mTextureVector[ID];

}

Mix_Chunk* ResourceManager::getMusic(const char* file)
{
	Mix_Chunk* music = NULL;
	return music;
}

Mix_Chunk* ResourceManager::getAudioByID(Sint32 ID)
{
	Mix_Chunk* music = NULL;
	std::vector<Mix_Chunk*>::iterator it = MusicVector.begin() + ID;
	if (it != MusicVector.end())
	{
		music = MusicVector[ID];
	}
	return music;

}

Sint32 ResourceManager::getAudioID(const char* file)
{
	Sint32 id = -1;
	std::map<std::string, Sint32>::iterator it = MusicIDMap.find(file);
	if (it != MusicIDMap.end())
	{
		id = it->second;
	}
	else
	{

		id = addMusic(file);
		MusicIDMap.insert(std::pair<std::string, Sint32>(file, id));
	}
	return id;

}

Mix_Chunk* ResourceManager::loadMusic(const char* path)
{
	Mix_Chunk* music = Mix_LoadWAV(path);
	if (music == NULL)
	{
		cout << "Error LoadMusic" << endl;
	}
	return music;

}
Sint32 ResourceManager::addMusic(const char* file)
{

	Sint32 id = 0;
	Mix_Chunk* music = loadMusic(file);
	if (music == NULL)
	{
		cout << "Error addMusic!" << endl;
	}
	else
	{
		if (MusicFirstFreeSlot == MusicVector.size())
		{
			MusicVector.push_back(music);
			id = MusicFirstFreeSlot;
		}
		else
		{
			MusicVector[MusicFirstFreeSlot] = music;
			id = MusicFirstFreeSlot;

		}
		std::map<std::string, Sint32>::iterator it = MusicIDMap.find(file);
		if (it != MusicIDMap.end())
		{
			MusicIDMap[file] = id;

		}
		else
		{
			MusicIDMap.insert(std::pair<std::string, Sint32>(file, id));
		}
		updateFirstFreeSlotMusic();
	}
	return id;

}
Uint32 ResourceManager::updateFirstFreeSlotMusic()
{
	Uint32 id = MusicVector.size();
	for (int i = 0; i < MusicVector.size(); i++)
	{
		if (MusicVector[i] == NULL)
		{
			id = i;
		}
	}
	MusicFirstFreeSlot = id;
	return MusicFirstFreeSlot;
}

void ResourceManager::setAlphaGraphic(Sint32 ID, Uint8 alpha_value)
{
	SDL_Texture* surf = NULL;
	surf = getGraphicByID(ID);
	if (surf != NULL)
	{
		SDL_SetTextureAlphaMod(surf, alpha_value);
	}
	else
	{
		cout << "Error setAlphaGraphic" << endl;
	}
}

Sint32 ResourceManager::addGraphic(const char* file)
{
	
	//The final texture
	SDL_Texture* newTexture = NULL;

	//Load image at specified path
	SDL_Surface* loadedSurface = IMG_Load(file);
	if (loadedSurface == NULL)
	{
		printf("Unable to load image %s! SDL_image Error: %s\n", file, IMG_GetError());
	}
	else
	{
		//Create texture from surface pixels
		newTexture = SDL_CreateTextureFromSurface(VideoManager::getInstance()->renderer, loadedSurface);
		if (newTexture == NULL)
		{
			printf("Unable to create texture from %s! SDL Error: %s\n", file, SDL_GetError());
		}

		//Get rid of old loaded surface
		SDL_FreeSurface(loadedSurface);
	}

	Sint32 pos = -1;
	if (mFirstFreeSlot == mTextureVector.size())
		mTextureVector.push_back(NULL);
	mTextureVector[mFirstFreeSlot] = newTexture;
	mIDMap.emplace(file, mFirstFreeSlot);
	pos = mFirstFreeSlot;
	updateFirstFreeSlotGraphic();
	
	return pos;

}

void ResourceManager::removeGraphic(const char* file)
{

	std::map<std::string, Sint32>::iterator it = mIDMap.find(file);
	
	if (it != mIDMap.end())
	{
		mIDMap.erase(it);
		
		
		updateFirstFreeSlotGraphic();
	}
	else
	{
		cout << "Error removeGraphic" << endl;
	}
}

Uint32 ResourceManager::updateFirstFreeSlotGraphic()
{
	Uint32 id = mTextureVector.size();
	for (int i = 0; i < mTextureVector.size(); i++)
	{
		if (mTextureVector[i] == NULL)
		{
			id = i;
		}
	}
	mFirstFreeSlot = id;
	return mFirstFreeSlot;
}


std::string ResourceManager::getGraphicPathByID(Sint32 ID)
{
	std::string path;
	std::map<std::string, Sint32>::iterator it;
	for (it = mIDMap.begin(); it != mIDMap.end(); it++)
	{
		if (it->second == ID)
		{
			path = it->first;
		}
	}
	return path;

}

void ResourceManager::printLoadedGraphics()
{
	std::map<std::string, Sint32>::iterator it;
	for (it = mIDMap.begin(); it != mIDMap.end(); it++)
	{
		cout << it->second << " -- " << it->first << endl;
	}
}

Sint32 ResourceManager::createGraphic(const char* file, Uint16 width, Uint16 height)
{

	Uint32 rmask, gmask, bmask, amask;
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
	rmask = 0xff000000;
	gmask = 0x00ff0000;
	bmask = 0x0000ff00;
	amask = 0x000000ff;
#else
	rmask = 0x000000ff;
	gmask = 0x0000ff00;
	bmask = 0x00ff0000;
	amask = 0xff000000;
#endif
	SDL_Surface* surf = NULL;
	int idpath = 0;
	surf =  SDL_CreateRGBSurface(0,width,height, 32,
		rmask,gmask,bmask, amask);
	/*idpath = IMG_SavePNG(surf,file);
	if (idpath != -1)
	{	
		id=addGraphic(file);
	}*/
	Uint32 id = updateFirstFreeSlotGraphic();
	mGraphicsVector.push_back(surf);
	mIDMap.insert(std::pair<std::string, Sint32>(file, id));
	return id;

}




ResourceManager::~ResourceManager()
{
	mGraphicsVector.clear();
}
